import java.util.ArrayList;

public class CountAway {

    public static void main ( String[] args) {
       
       int children, gap, counter;
       ArrayList<Integer> list = new ArrayList<Integer>();
       children = 20;
       gap = 5;
       String[] teams = new String[21];
       
       for (int count = 1; count <= children; count++) {
         list.add(new Integer(count));
       }
       counter = gap - 1;
       
       for (int i = 1; i < 21; i++) {
    	   teams[i] = "G";
       }

       System.out.println("The children were originally arranged like this: ");

       while (!(list.isEmpty())) {
    	   	int c = list.remove(counter);
   	   		teams[c] = "B";
   	   		children = children - 1;
   	   		if (children > 0)
            counter = (counter + gap - 1) % children;
   	   		if (children <= 10)
   	   			break;
       }
 
       for (int i = 1; i < 21; i++) {
    	   System.out.print(teams[i] + " ");
       }
   }
}